﻿using Microsoft.EntityFrameworkCore;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddHealthChecks(); // Register health check
builder.Services.AddControllers();

string conn = "Data Source=LAPTOP-NR588SC6\\SQLEXPRESS;Database=HealthCheckDb;Trusted_Connection=True;TrustServerCertificate=True";

builder.Services.AddDbContext<AppDbContext>(x => x.UseSqlServer(conn)); // Đăng ký sử dụng EF

WebApplication app = builder.Build();

app.UseHealthChecks("/healthcheck-api"); // heal check app

app.UseMiddleware<DatabaseHealthCheckMiddleware>(); // Custom middleware

app.MapControllers();

app.Run();

public class DatabaseHealthCheckMiddleware
{
    private readonly RequestDelegate _next;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    public DatabaseHealthCheckMiddleware(RequestDelegate next, IServiceScopeFactory serviceScopeFactory)
    {
        _next = next;
        _serviceScopeFactory = serviceScopeFactory;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        if (httpContext.Request.Path == "/healthcheck-sql")
        {
            try
            {
                using (IServiceScope scope = _serviceScopeFactory.CreateScope())
                {
                    AppDbContext dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
                    await dbContext.Database.ExecuteSqlRawAsync("SELECT 1"); // thử kết nối tới database
                    httpContext.Response.StatusCode = 200;
                    await httpContext.Response.WriteAsync("Database connected");
                }
            }
            catch (Exception ex)
            {   // nếu như không thể kết nối tới database sẽ có lỗi
                Console.WriteLine($"Message: {ex.Message}");
                httpContext.Response.StatusCode = 500;
                await httpContext.Response.WriteAsync("Database unhealthy");
            }
        }
        else
        {
            await _next(httpContext);
        }
    }
}


