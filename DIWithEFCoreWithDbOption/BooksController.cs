﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DIWithEFCoreWithDbOption
{
    [ApiController]
    [Route("api/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly AppDbContext _dbContext;

        public BooksController(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public async Task<List<Book>> Books()
        {
            return await _dbContext.Books.ToListAsync();
        }

        [HttpPost]
        public async Task<IActionResult> CreateBook(BookDTO book)
        {
            Book newBook = new Book()
            {
                Title = $"{book.Title} {Guid.NewGuid()}",
            };

            _dbContext.Books.Add(newBook);
            int rowEffect = await _dbContext.SaveChangesAsync();

            if (rowEffect == 1) // inssert dữ liệu thành công
            {
                return Ok("Success");
            }

            return BadRequest("Failed");
        }

        [HttpGet("{id}")]
        public async Task<Book?> Book(int id)
        {
            return await _dbContext.Books.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveBook(int id)
        {
            Book book = await _dbContext.Books.FirstOrDefaultAsync(x => x.Id == id);

            if (book is null)
            {
                return NotFound($"not found book id: {id}");
            }

            _dbContext.Books.Remove(book);
            int rowEffect = await _dbContext.SaveChangesAsync();

            if (rowEffect == 1)
            {
                return Ok("Success");
            }

            return BadRequest("Failed");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBook([FromRoute] int id, [FromBody] BookDTO bookUpdate)
        {
            Book book = await _dbContext.Books.FirstOrDefaultAsync(x => x.Id == id);

            if (book is null)
            {
                return NotFound($"not found book id: {id}");
            }

            book.Title = bookUpdate.Title;
            _dbContext.SaveChanges();

            return Ok("Updated");
        }
    }
}