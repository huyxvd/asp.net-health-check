﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    /// <summary>
    /// Sử dụng Dependency injection truyền options lên
    /// </summary>
    /// <param name="options"></param>
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public DbSet<Book> Books { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Tạo dữ liệu giả mạo
        modelBuilder.Entity<Book>().HasData(
            new Book { Id = 1, Title = "Title 1" },
            new Book { Id = 2, Title = "Title 2" },
            new Book { Id = 3, Title = "Title 3" }
        );
    }

}
